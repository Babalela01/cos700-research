import os
import pickle

from processing.common import positions, filename, chains, data_folder, load_pickle

def read_pickle(_algorithm, _chain, _position):
    return load_pickle(data_folder + filename(_algorithm, _chain, _position))


def get_position(by_run):
    i_p = 1
    i_f = 0
    fitness = by_run[0][-1][i_f]
    pos = by_run[0][-1][i_p]
    for run in by_run:
        newf = run[-1][i_f]
        if newf < fitness:
            fitness = newf
            pos = run[-1][i_p]
    return pos


def jacobian_pos(chain, ip):
    position = positions[ip]
    by_run = read_pickle("Jacobian", chain, position)
    return get_position(by_run)


# best Fitness, best position, average fitness, cure best Particle, cure best fitness
def pso_pos(chain, ip):
    position = positions[ip]
    by_run = read_pickle("PSO", chain, position)
    return get_position(by_run)


folder = "../results/positions"
os.makedirs(folder, exist_ok=True)
for c in chains:
    res = [(pso_pos(c, ip), jacobian_pos(c, ip)) for ip in range(len(positions))]
    with open("%s/%s_joints.pickle" % (folder, c), "wb") as f:
        pickle.dump(res, f, protocol=pickle.HIGHEST_PROTOCOL)


print("done")

