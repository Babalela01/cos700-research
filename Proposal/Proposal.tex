% !TeX spellcheck = en_GB
\documentclass[a4paper,titlepage]{article}
\usepackage[margin=1in]{geometry}
\usepackage[numbers,sort&compress,elide]{natbib}
\usepackage{graphicx}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{wrapfig}
\usepackage{parskip}
\usepackage[affil-it]{authblk}

\renewcommand{\bibsection}{\section{References}\label{sec:references}}

\usepackage[hidelinks]{hyperref}

\graphicspath{{images/}}
% Title Page
\title{Solving Inverse Kinematics Using Particle Swarm Optimization \\
	\hfill \\
	\large COS700 Research Proposal}
\author{%Renette Ros
	13007557 \\ \hfil \\ 
	Supervisor: Dr M. Helbig}
\affil{Department of Computer Science \\ 
	University of Pretoria \\}
\begin{document}

\maketitle

\begin{abstract}
	Inverse kinematics (IK) is a fairly old problem in robotics and computer animation that aims to compute the joint angles needed to reach a specific pose. The inverse kinematics problem can have multiple, equally fit solutions or no solutions at all. Various methods have been proposed to iteratively approximate the IK solution, but these methods yield only one solution and perform poorly in some situations where the target position is unreachable. The aim of this research is to develop a Particle Swarm Optimization (PSO) to solve the IK problem for an animated human.  The performance and solution quality of the PSO will then be compared to some of the traditional, matrix-based methods. 
\end{abstract}

\section{Introduction}

\subsection{Forward and Inverse kinematics}
In computer animation, a skeleton can be represented as a collection of links or bones connected by rotational joints. This model is a hierarchical structure where every link is positioned relatively to a previous link in the chain. As illustrated in \figurename~\ref{fig:ikfk}, the model can be manipulated using either forward (manipulating the joints) or inverse (manipulating the end effectors) kinematics.

%Explain DOF?

\begin{figure}[!ht]
	\centering
	\begin{subfigure}{0.27\textwidth}
		\includegraphics[width=\textwidth]{FK.png}
		\subcaption{}\label{fig:fk}
	\end{subfigure}
	~
	\begin{subfigure}{0.27\textwidth}
		\includegraphics[width=\textwidth]{IK.png}
		\subcaption{}\label{fig:ik}
	\end{subfigure}
	\caption{Forward (\subref{fig:fk}) and inverse (\subref{fig:ik}) kinematics as illustrated by \cite{berkaikbasics}}
	\label{fig:ikfk}
\end{figure}
 
The goal of the forward kinematics problem, as illustrated in \figurename~\ref{fig:fk}, is to find the position of the end effector based on the transformations of the joints. In contrast, the goal of inverse kinematics is to find appropriate joint transformations to place the end effector (the last element in the chain) at a desired target position (\figurename~\ref{fig:ik}). 

\subsection{Solving inverse kinematics}\label{subsec:solvingik} 

Aristidou \cite{aristidou2009inverse} and Buss \cite{buss2004intro} describes inverse kinematics mathematically as follows:
Let $ \vec{\theta} = \theta_1 ... \theta_n $ be the joint configurations for the entire model and $ \vec{s} =  s_1 ... s_k $ the positions of the end effectors. The forward kinematics equation can then be expressed as 
\begin{equation} \label{eq:fk}
 \vec{s} = f(\vec{\theta})
\end{equation}
and inverse kinematics as 
\begin{equation} \label{eq:ik}
	\vec{\theta} = f^{-1}(\vec{t})
\end{equation}
for desired end positions $\vec{t} = t_1 ... t_k $. This numerical approach has various problems \cite{berkaikbasics,buss2004intro,aristidou2009inverse, welman1989figures}:
\begin{itemize}
	\item There might be no solutions (if the target position is out of reach)
	\item There can be multiple, equally fit solutions
	\item f is non-linear and difficult to invert
\end{itemize}

Another popular method for solving inverse kinematics is by using the Jacobian matrix to iteratively move the end-effector positions ($\vec{s}$) closer to the target positions ($\vec{t}$) \cite{aristidou2009inverse, aristidou2011fabrik,buss2004intro, welman1989figures}. The Jacobian matrix can be expressed as a function of the $\theta$ values:
\begin{equation}
	J(\vec{\theta}) = \left(\frac{\partial{s_i}}{\partial{\theta_j}}\right)_{ij}
\end{equation}
\citet{buss2004intro} and others \cite{aristidou2009inverse,welman1989figures} show that the position change at each iteration ($\Delta s$)  is given by Equation \ref {eq:deltas}:
\begin{equation}\label{eq:deltas}
	\Delta s = J\Delta\theta 
\end{equation}
In Equation \ref{eq:deltas}, $\Delta \theta$ represents the change in joint configurations. Solving Equation \ref{eq:deltas} for $\Delta\theta$ gives
\begin{equation}\label{eq:deltathe}
	\Delta \theta = J^{-1}\Delta s
\end{equation} 
The position change in Equation \ref{eq:deltathe} can be calculated as the difference between the target and current positions ($\Delta s = \vec{t} - \vec{s}$).
The problem with this approach is that the Jacobian matrix might not be invertible, or even square, and that only one solution will be found, regardless of how many exist. Due to its iterative nature, the Jacobian based approach can take very long to converge \cite{wang1991ccd}

Different methods that have been used to calculate, approximate and replace the inverse matrix will be discussed in Section \ref{sec:survey}.  

\subsection{Particle Swarm optimization}
Particle Swarm optimization (PSO) is a population-based search algorithm based on the social behaviour of birds in a flock \cite{eberhart1995PSO}. In the PSO, potential solutions are represented as particles that move around in the problem's search space. Particle movement is based on the experience of the particle itself and the experiences of its neighbours \cite{apengel}. 

In a PSO,  every particle has a position and a velocity which is initialised randomly \cite{eberhart1995PSO}. At every time step the particles move around in the search space and their velocity is recalculated. Velocity is based on their personal best position, a neighbourhood best position and two random weights \cite{eberhart1995PSO, apengel}.


\subsection{This Proposal}
The focus of this research is on solving the inverse kinematics for an animated humanoid with a PSO and comparing it to the traditional Jacobian-based approaches. 

The structure of this proposal is as follows: Section \ref{sec:problem} will define the problem that this research is addressing as well as the limits of the  research. Section \ref{sec:survey} is a review of existing literature on inverse kinematics and PSOs. Sections \ref{sec:method} and \ref{sec:planning} will describe the research methodology and give an outline of the necessary work. A list of references is included in Section \ref{sec:references} at the end of the paper. 

\section{Problem Statement} \label{sec:problem}
Existing numerical and Jacobian-based approaches to solving inverse kinematics are computationally expensive, can have unsatisfying results \cite{tevatia2000humanoids} and can perform poorly in situations where the target is unreachable or the Jacobian matrix is close to singular (the determinant is close to 0) \cite{buss2004intro}. 

This work will use a PSO to solve inverse kinematics for an animated humanoid character and compare it to existing methods. 

\subsection{Objectives}
\begin{enumerate}
	\item Create a human model with a skeleton 
	\begin{itemize}
		\item Model the human
		\item Create the skeleton
		\item Define constraints for the joints
	\end{itemize}
	\item Develop a PSO algorithm to solve the inverse kinematics
	\begin{itemize}
		\item Encode joint configurations as particles
		\item Choose a fitness measure to evaluate candidate solutions.
		\item Handle joint constraints
	\end{itemize}
	\item Solve IK for a specific animations
	\begin{itemize}
		\item The animation defines target positions
		\item Solve IK using the PSO
		\item Solve IK using matrix-based approaches
	\end{itemize}
	\item Compare the different IK solutions
	\begin{itemize}
		\item Quantify how realistic movement is
		\item Choose fitness measures to compare the solutions
	\end{itemize}

\end{enumerate}

\subsection{Limits}
This research will not investigate all existing IK algorithms. Two  Jacobian-based approaches - the Jacobian transpose and Moore-Penrose pseudo-inverse methods - will be compared to the PSO solver. 

\section{Literature Survey}\label{sec:survey}
\subsection{Inverse Kinematics}
Inverse kinematics (IK) originated from robotics \cite{buss2004intro} but it has also been applied to computer animation \cite{aristidou2009inverse}. Some difference between the applications of inverse kinematics in the fields of robotics and computer animation is that computer models can be more complex with larger degrees of freedom \cite{welman1989figures, zhao1994nonlinear}. IK in computer animation is most commonly applied to animating humans or other creatures \cite{buss2004intro}.

A popular iterative approach to solving inverse kinematics uses the Jacobian matrix (a matrix of partial derivatives of the entire system) \cite{aristidou2009inverse}, which gives linear approximations to the IK problem \cite{aristidou2009inverse, berkaikbasics} as described in Section \ref{subsec:solvingik}. Orin \cite{orin1984jacobian} introduced and compared several different methods for efficiently calculating the Jacobian for different joint types. 

Because the Jacobian is not always invertible, various methods to calculate or approximate the inverse Jacobian have been proposed. One technique  is to simply use the transpose of the Jacobian instead of the inverse  \cite{balestrino1984robust, wolovich1984computational}. This method eliminates the need for computationally expensive matrix inversions \cite{pechev2008noinverse} and gives predictable results \cite{welman1989figures}  but it does not perform well situations where the Jacobian matrix is close to singular or the target is unreachable \cite{buss2004intro,pechev2008noinverse}. 

A generalised inverse of the Jacobian can also be used \cite{buss2004intro,welman1989figures}. One generalized inverse that is commonly used is the Moore-Penrose Inverse \cite{buss2004intro, buss2005selectively}. Klein and Huang \cite{klein1983pseudoinverse} showed that this pseudo inverse (for a non-square matrix) will result in as little joint movement as possible. In situations where the target position is not reachable or the Jacobian matrix is singular, the pseudo inverse can be clamped to prevent large changes in joint angles \cite{buss2005selectively}.  Baillieul \cite{baillieul1985kinematic} proposed the extended Jacobian method that tracks a local minimum, to eliminate the problem of singular matrices. 

The Jacobian based solutions aim to solve all joint positions at the same time,  making the incorporation of constraints more difficult. \citet{welman1989figures} and \citet{buss2005selectively} stated that simply clamping joints to their limits often give adequate results in practice, but can also lead to unexpected configurations. \citet{welman1989figures} also investigated the use of a matrix based penalty function to prevent constraints from being  violated. 

The damped least squares (DLS) method was first used for IK by Wampler \cite{wampler1986manipulator} and Nakamura and Hanafusa \cite{nakamura1986inverse}. DLS is based on the Jacobian method \cite{buss2004intro} but uses a damping factor to prevent infeasible joint configurations. DLS's damping sacrifices some precision \cite{deo1995dls}. Buss \cite{buss2005selectively} proposed a generalization of DLS called selectively damped least squares (SDLS) that selectively dampens values based on the difficulty of reaching a target. 

\citet{wang1991ccd} proposed a heuristic based approach, cyclic coordinate descent (CCD), that aims to solve all joints individually. At every iteration, CCD traverses the kinematic chain from end to base and updates joints individually based on a heuristic function  \cite{welman1989figures, lander1998kine}. This algorithm is guaranteed to converge, but convergence time is hard to estimate \cite{welman1989figures}. Introducing local, joint based constraints into CCD is relatively simple as individual joint configurations can be clamped before the next joint is changed \cite{wang1991ccd,welman1989figures, lander1998kine}. 

Non matrix based approaches has also been used to solve inverse kinematics. Guez and Ahmad \cite{guez1988neuralnet} used a neural network to solve the IK problem in robotics and genetic algorithms has also been used to solve the IK problem \cite{parker1989ga}. 

\subsection{PSO}
Particle swarm optimization (PSO) was first proposed by \citet{eberhart1995PSO, kennedy1995PSO} in 1995. The original PSO had two user determined variables: An acceleration constant and a maximum velocity \cite{eberhart1995PSO}.  \citet{shi1998inertia} introduced an inertia weight component to  remove the necessity for a maximum velocity and improve control over the search.  Various strategies were also developed to adjust the inertia weight over time for better performance \cite{poli2007particle}. \citet{clerc1999constriction} developed a similar, mathematically equivalent, approach \cite{apengel} that uses an constriction coefficient instead of the inertia weight. \citet{clerc2002coefficients} analysed different ways to implement the constriction coefficients and suggested common values to ensure convergence.  

Kennedy and Eberhart \cite{eberhart1995PSO, kennedy1995PSO} introduced the gbest (global best) and lbest (local best) topologies with the original PSO, but other dynamic and static topologies and their effect on the search results have been investigated \cite{de2009frankenstein,  kennedy1999topology, mendes2004topology, peram2003topology, poli2007particle, suganthan1999topology}.

Various strategies have been proposed to handle boundary constrained problems in PSOs. The simplest approaches are to not consider infeasible solutions for best positions or to reinitialize them with random values \cite{apengel}. Other constraint handling approaches include penalty functions \cite{parsopoulos2002particleconstraints}, repair methods \cite{hu2002constraint} or replacing infeasible particles with their personal best positions immediately \cite{elgallad2002params}.

PSOs have been applied to a wide range of problems \cite{kennedy2011applications,poli2007particle}, including inverse kinematics for robotics \cite{huang2012particle, rokbani2012pso, wen2008psohybrid}, training neural networks \cite{kennedy2011applications} and single-  and multi-objective  optimization problems \cite{ delValle2008PSOmoo, apengel, kennedy2011applications}.

\section{Methodology} \label{sec:method}

The main approach this research will use is experimentation. 
\subsection{Research}
Before the inverse kinematics can be implemented, more research needs to be conducted into different constraint handling approaches, fitness measures for the PSO and fitness measures for comparison and analysis. To make motion more realistic, joint constraints for the model skeletons must be based on research. 

\subsection{Modelling}
A 3D model of a human will be created using the open-source Blender software package. The model will have a skeleton with appropriate joint constraints. The movement of this model and its limbs will be used to evaluate the different IK algorithms. The IK algorithms will also be evaluated on a model with different constraints.

\subsection{Programming}
Two configurable IK solvers will be implemented for Blender (Blender can be extended through Python scripts).  The first  solver will  implement IK with a Jacobian matrix as described in Section \ref{subsec:solvingik}. The solver will be able to use different matrix inversion techniques. The techniques that will be examined is the Jacobian transpose and the Moore-Penrose pseudo-inverse. 
The second solver will implement a PSO with configurable parameters (neighbourhood, coefficients, swarm size) to solve the model's inverse kinematics. In the PSO, the particles will represent a set of configurations (joint angles) for the model. 

Specific fitness measures must also be implemented to quantify and compare the PSO particles (possible solutions) and the results of the different IK algorithms.

\subsection{Analysis}
After the IK has been solved the different results will be compared.  Some fitness measures that will be used include how close the end-effectors are to their targets, how much the individual joints had to move for the end effector to reach the position and how realistic motion is for unreachable positions. 

\section{Planning} \label{sec:planning}
\subsection{Tasks} \label{subsec:tasks}
The project has been divided into the following tasks and subtasks:
\begin{itemize}
	\item Research
	\begin{itemize}
		\item How Blender scripting works
		\item Using Blender scripts for IK
		\item Constraint Handling
		\item IK fitness measures
		\item Appropriate joint constraints for human models
	\end{itemize}
	\item Construct Models
	\begin{itemize}
		\item Create humanoid models from reference images
		\item Add skeletons to the models
		\item Set up joint constraints
		\item Update report
	\end{itemize}
	\item Implement and debug the IK solvers
	\begin{itemize}
		\item Jacobian based  solver with transposed and pseudo-inverse matrices
		\item PSO solver with configurable control parameters
		\item Research: Constraint Handling
		\item Research: Fitness Measures
		\item Document relevant decisions in report
	\end{itemize}
	\item Define the animation
	\begin{itemize}
		\item Define target positions
		\item Add additional constraints
		\item Document details
	\end{itemize}
	\item Run the simulations for the different IK solvers
	\item Analyse and interpret results
	\begin{itemize}
		\item Get results in correct format
		\item Apply statistical test if possible
		\item Add results and conclusions to report
	\end{itemize}
	\item Finish report 
	\begin{itemize}
		\item Abstract
		\item Conclusion
		\item Other missing sections
		\item Proofread and edit report
	\end{itemize}
	\item Time for supervisor to read and give feedback on final report
	\item Report editing after supervisor feedback
\end{itemize}

\subsection{Time Estimation}


The Gantt chart in \figurename~\ref{fig:gantt} shows the time estimates for the main tasks described in Section \ref{subsec:tasks}:
\begin{figure}[!ht]
	\includegraphics[width=\textwidth]{gantt.png}
	\caption{A gantt chart for the project tasks. } \label{fig:gantt}
\end{figure}


\bibliographystyle{plainnat}
\bibliography{bibliography}


\end{document}          
