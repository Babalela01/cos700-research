# See Buss 2004 Intro for explanation
#  t = position
#  pheta = Joint angles
#
#
import math
from typing import List, Tuple

import numpy as np

import bpy
from common import get_bone, get_armature, bone_chains, get_bone_list, reset_model, distance, \
    world_matrix, update_pose
from mathutils import Vector, Matrix
from pso.particle import Constraints


def unit_vecs(bone: bpy.types.PoseBone, axis: int) -> Vector:
    return {
        0: bone.x_axis,
        1: bone.y_axis,
        2: bone.z_axis
    }[axis]


# Moore-Penrose Pseudo Inverse
mp_pseudo_inverse = np.linalg.pinv
# Transpose
transpose = np.transpose


def clamp(angle: float, boundary: List[float]):
    """
    Clamps angle to boundary
    :param angle:
    :param boundary:
    :return:
    """
    if angle < Constraints.MIN:
        angle += Constraints.FULL
    elif angle > Constraints.MAX:
        angle -= Constraints.FULL

    maxv = boundary[1]
    minv = boundary[0]
    return max(minv, min(maxv, angle))


class Jacobian:
    def get_joint(self, bone_tuple: Tuple[bpy.types.PoseBone, int]) -> Vector:
        bone = bone_tuple[0]
        axis = bone_tuple[1]

        # vj = Unit vector pointing along Rotation axis in world coordinates
        vj = unit_vecs(bone, axis)
        # vj.normalize()

        # si = end effector position
        si = self.end_effector.tail
        # pj = joint position
        pj = bone.head
        diff = si - pj

        return vj.cross(diff)

    def __init__(self, armature_name='Armature'):
        self.dist = None
        self.armature = get_armature(armature_name)
        self.world_matrix = world_matrix(self.armature)
        self.end_effector = None
        self.target = None
        self.bone_list = None
        self.invert = None
        self.boundaries = None
        self.iteration_index = 0

    def local_coordinates(self, v: Vector) -> Vector:
        return self.world_matrix.inverted() * v

    def global_coordinates(self, v: Vector):
        return self.world_matrix * v

    @staticmethod
    def run_chain(target: List[float], chain: str, run_count: int, **kwargs):
        jacobian = Jacobian()
        return tuple(jacobian.run(target, bone_chains[chain], bone_chains[chain + "_end"], **kwargs)
                     for _ in range(run_count))

    def run(self, target, bone_name_list: List[Tuple[str, str, Tuple[float, float]]], end_name,
            invert=mp_pseudo_inverse, max_iterations=20):
        from random import uniform
        reset_model(self.armature)
        self.end_effector = get_bone(end_name, self.armature)
        self.target = self.local_coordinates(Vector(target))
        self.bone_list = get_bone_list(bone_name_list, self.armature)
        self.invert = invert
        self.boundaries = [b[2] for b in bone_name_list]

        # angles = Vector(get_rotation(*bt) for bt in self.bone_list)
        angles = Vector(uniform(*self.boundaries[i]) for i in range(len(self.bone_list)))
        dist = update_pose(self.bone_list, angles, self.end_effector.tail, self.target)

        # bone list
        # 0 -> name
        # 1 -> axis
        # 2 -> boundary
        self.iteration_index = 0
        iteration_data = []
        app_iterattion = iteration_data.append
        while self.iteration_index < max_iterations:
            angles = self.iteration(angles)
            dist = update_pose(self.bone_list, angles, self.end_effector.tail, self.target)
            app_iterattion((dist, tuple(angles)))
            self.iteration_index += 1
            print("Jacobian Iteration #%d: %.2f" % (self.iteration_index, dist))
        print("Jacobian Iteration #%d: %s %.2f" % (self.iteration_index, str(self.end_effector.tail), dist))
        return iteration_data
        # boundaries = [b[2] for b in bone_name_list]
        # bone_list = [(get_bone(b[0], self.armature), get_axis(b[1])) for b in bone_name_list]

    def iteration(self, angles: Vector) -> Vector:
        update_pose(self.bone_list, angles)
        matrix = self.get_jacobian(self.bone_list)
        # Inverted Jacobian Matrix N x 3
        inverted = self.invert(matrix)
        angle_update = inverted.dot(self.target)
        angles = angles + Vector(angle_update)
        return Vector([clamp(a, b) for a, b in zip(angles, self.boundaries)])

    def get_jacobian(self, bone_name_list) -> Matrix:
        return np.transpose(
            [self.get_joint(bone) for bone in bone_name_list]
        )
