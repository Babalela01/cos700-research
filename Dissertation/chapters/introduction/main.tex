%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\glsresetall
\renewcommand{\folder}{chapters/introduction/}
\chapter{Introduction}
\label{chap:introduction}
\pagestyle{headings}
\pagenumbering{arabic}
\setcounter{page}{1}
This chapter introduces the problem this research attempts to solve and describes the motivation and objectives of this research. 

\begin{itemize}
	\item \autoref{sec:introduction:ik} introduces the \acrlong{ik} problem and existing solutions. 
	\item \autoref{sec:introduction:pso} gives a brief overview of the \acrlong{pso} algorithm. 
	\item \autoref{sec:introduction:motivation} describes the motivation for conducting this research. 
	\item \autoref{sec:introduction:objectives} lists the research objectives. 
	\item \autoref{sec:introduction:outline} gives an outline for the remainder of this document. 
\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%TODO struvture
\section{Forward and Inverse Kinematics}\label{sec:introduction:ik}
\summary{This section introduces the \acrlong{ik} problem and describes some existing solutions and their shortcomings.  }

In computer animation, a skeleton can be represented as a collection of links or bones connected by rotational joints. This model is a hierarchical structure where every link is positioned relatively to a previous link in the chain. As illustrated in \autoref{fig:ikfk}, the model can be manipulated using either forward (manipulating the joints) or inverse (manipulating the end effectors) kinematics.

%Explain DOF?

\begin{figure}[!ht]
	\centering
	\begin{subfigure}{0.27\textwidth}
		\includegraphics[width=\textwidth]{\folder/figures/FK.png}
		\subcaption{}\label{fig:fk}
	\end{subfigure}
	~
	\begin{subfigure}{0.27\textwidth}
		\includegraphics[width=\textwidth]{\folder/figures/IK.png}
		\subcaption{}\label{fig:ik}
	\end{subfigure}
	\caption{Forward (\subref{fig:fk}) and inverse (\subref{fig:ik}) kinematics \cite{berkaikbasics}}.
	\label{fig:ikfk}
\end{figure}

The goal of the \gls{fk} problem, as illustrated in \autoref{fig:fk}, is to find the position of the end effector based on the transformations of the joints. In contrast, the goal of \gls{ik} is to find appropriate joint transformations to place the end effector (the last element in the chain) at a desired target position (refer to \autoref{fig:ik}).

The following subsections describe existing approaches to solving \gls{ik} and some of their shortcomings.  

\subsection{Solving Inverse Kinematics}\label{subsec:solvingik} 


\citet{aristidou2009inverse} and \citet{buss2004intro} describe inverse kinematics mathematically as follows:
Let $ \vec{\theta} = \theta_1 ... \theta_n $ be the joint configurations for the entire model and $ \vec{s} =  s_1 ... s_k $ the positions of the end effectors. The forward kinematics equation can then be expressed as 
\begin{equation} \label{eq:fk}
\vec{s} = f(\vec{\theta})
\end{equation}
and inverse kinematics as 
\begin{equation} \label{eq:ik}
\vec{\theta} = f^{-1}(\vec{t})
\end{equation}
for desired end-effector positions $\vec{t} = t_1 ... t_k $. \\

There are various problems with trying to solve \gls{ik} by simply inverting $f$ \cite{berkaikbasics,buss2004intro,aristidou2009inverse, welman1989figures}:
\begin{itemize}
	\item There might be no solutions (if the target position is out of reach).
	\item There can be multiple, equally fit solutions.
	\item f is non-linear and difficult to invert. 
\end{itemize}

A popular method for solving inverse kinematics is by using the Jacobian matrix to iteratively move the end-effector positions ($\vec{s}$) closer to the target positions ($\vec{t}$) \cite{aristidou2009inverse, aristidou2011fabrik,buss2004intro, welman1989figures}. The Jacobian matrix can be expressed as a function of the $\theta$ values:
\begin{equation}\label{eq:jacobiandefinition}
J(\vec{\theta}) = \left(\frac{\partial{s_i}}{\partial{\theta_j}}\right)_{ij}
\end{equation}
\citet{buss2004intro} and others \cite{aristidou2009inverse,welman1989figures} show that the position change at each iteration ($\Delta s$)  is given by \autoref {eq:deltas}:
\begin{equation}\label{eq:deltas}
\Delta\vec{s} = J\Delta\vec{\theta}
\end{equation}
In  \autoref{eq:deltas}, $\Delta \theta$ represents the change in joint configurations. Solving  \autoref{eq:deltas} for $\Delta\theta$ gives
\begin{equation}\label{eq:deltathe}
\Delta\vec{\theta} = J^{-1}\Delta\vec{s}
\end{equation} 
The position change ($\Delta s$) in \autoref{eq:deltathe} can be calculated as the difference between the target and current positions ($\Delta s = \vec{t}-\vec{s}$).



%Different methods that have been used to calculate, approximate and replace the inverse matrix will be discussed in \autoref{sec:survey}.  


\subsection{Problems with Jacobian Approaches}
Redundant manipulators are manipulators that have higher \gls{dof} than is required to accomplish the positioning task \cite{welman1989figures}. Redundant manipulators have rectangular (therefore non-invertible) Jacobian matrices and the \gls{ik} problem can have an infinite number of solutions. 

Additional problems with the Jacobian approach is that even if the Jacobian matrix is square it  might not be invertible, and that only one solution will be found, regardless of how many exist. Furthermore, due to its iterative nature, the Jacobian based approach can take very long to converge \cite{wang1991ccd}.

\section{Particle Swarm Optimization}\label{sec:introduction:pso}
\summary{This section gives a brief overview of the \acrlong{pso} algorithm.} 

\gls{pso} is a swarm intelligence algorithm based on the social behaviour of birds in a flock \cite{eberhart1995PSO}. In the \gls{pso}, potential solutions are represented as particles that move around in hyperspace. Particle movement is based on the experience of the particle itself (the cognitive component) and the experiences of its neighbours (the social component) \cite{apengel, kennedy1995PSO}. 

In a \gls{pso} candidate solutions are encoded as particles that move around in social space. Particles have a position (a candidate solution) which is initialised randomly and a velocity that is used to update the position. At every time step the particles move around in the search space and their velocity is recalculated. Velocity is based on their personal best position, a neighbourhood best position and two random weights \cite{eberhart1995PSO, apengel}.
%,  every particle has a position and a velocity which is initialised randomly \cite{eberhart1995PSO}. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%TODO 
\section{Motivation}
\label{sec:introduction:motivation}
\summary{This section describes the motivations for conducting this research. }

 Existing numerical and Jacobian-based approaches commonly used to solve inverse kinematics are computationally expensive, can have unsatisfying results \cite{tevatia2000humanoids} and can perform poorly in situations where the target is unreachable or the Jacobian matrix is close to singular (the determinant is close to 0) \cite{buss2004intro}. 
 
 This work uses a \gls{pso} to solve inverse kinematics for an \gls{3d} human character and compares the \gls{pso} to existing methods. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Objectives and Limits}
\label{sec:introduction:objectives}
This section describes the objectives (\autoref{sec:introduction:subobjectives}) and limits (\autoref{sec:introduction:limits}) of this research.

\subsection{Objectives} 
\label{sec:introduction:subobjectives}
This research uses a PSO and a Jacobian matrix to solve inverse kinematics for a \gls{3d} human model. The objectives of this research are: \pagebreak[3]
\begin{enumerate}
	\item Create a human model with a skeleton.  \begin{itemize} \item Model the human.
		\item Create the skeleton.
		\item Define constraints for the joints.
	\end{itemize}
	\item Develop a PSO algorithm to solve the inverse kinematics.
	\begin{itemize}
		\item Encode joint configurations as particles.
		\item Choose a fitness measure to evaluate candidate solutions.
		\item Handle joint boundary constraints.
	\end{itemize}
	\item Solve IK for a specific target positions.
	\begin{itemize}
		\item Solve IK using the PSO.
		\item Solve IK using a matrix-based approach.
	\end{itemize}
	\item Compare the different IK solutions.
	\begin{itemize}
		\item Choose fitness measures to compare the solutions.
		\item Perform a statistical test to determine significance of differences. 
		\item Determine possible reasons for performance differences.
	\end{itemize}
	
\end{enumerate}

\subsection{Limits}
\label{sec:introduction:limits}
This research does not investigate all existing IK algorithms. A  Jacobian-based approach using the Moore-Penrose pseudo-inverse methods is compared to a PSO solver. 

The only constraints that this research considers is boundary constraints for rotational joints. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{comment}
\section{Contributions}
\label{sec:introduction:contributions}

Enumerate the novel contributions that your work sets out to make to the field. Include specific novel contributions to the field, such as taxonomies, or empirical results. These will be quite closely related to the objectives listed in \autoref{sec:introduction:objectives}. You may also use a bulleted list here.
\end{comment}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Outline}
\label{sec:introduction:outline}
\summary{This section describes the structure of the remainder of this dissertation.} 

\pagebreak[3]
The structure of the dissertation is as follows:

\begin{itemize}
	\item\textbf{\autoref{chap:survey}} contains a survey  of existing research on \gls{pso}, \gls{ik} and other \gls{ci} techniques used to solve \gls{ik}.
	
	\item\textbf{\autoref{chap:method}} focuses on the research methodology and describes the experiment including software used, algorithms and parameter settings.
	
	\item\textbf{\autoref{chap:results}} contains the results and its interpretation.
	
	\item\textbf{\autoref{chap:conclusions}} covers the conclusions and highlights possible further work.
	
	\item\textbf{\autoref{app:acronyms}} provides a list of the  acronyms used or newly defined in the course of this work, as well as their associated definitions.
	
	\item\textbf{\autoref{app:symbols}} lists and defines the mathematical symbols used in this work, categorised according to the chapters in which they appear.
\end{itemize}

The bibliography can be found on page \pageref{bibliography}. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%