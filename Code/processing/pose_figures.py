import os

from processing.common import algorithms
from processing.common import chains

document_template = r"""\begin{figure}[!htpb]
  %(sf)s
  \caption{%(c)s %(a)s final poses}\label{fig:poses:%(c)s:%(a)s}
\end{figure}
"""

subfig_template = r"""\begin{subfigure}{0.24\textwidth}
  \includegraphics[width=\textwidth]{\folder/renders/%(c)s/target_%(t)d_%(a)s}
  \caption{Target %(t)d}
\end{subfigure}"""
folder = "../results/poses/"
os.makedirs(folder, exist_ok=True)
fname_template = folder + r"%(c)s_%(a)s_poses.tex"
# a - algorithm
# t - target
# c - chain
# sf - subfigure

for chain in chains:
    for alg in algorithms:
        subfigures = []
        obj = {
            'a': alg,
            'c': chain,
        }
        filename = fname_template % obj
        for target in range(8):
            obj['t'] = target+1
            sf = subfig_template % obj
            subfigures.append(sf)
        subfigs = "\n".join(subfigures[:4]) + r" \\ " + "\n".join(subfigures[4:])
        doc = document_template % {'c': chain, 'a': alg, 'sf': subfigs}
        with open(filename, 'w') as f:
            f.write(doc)


