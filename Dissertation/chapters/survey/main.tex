%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\renewcommand{\folder}{chapters/survey/}
\chapter{Literature Survey}
\label{chap:survey}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This chapter surveys existing work related to PSOs and inverse kinematics:
\begin{itemize}
	\item \autoref{sec:survey:IK} is a survey of existing research on \gls{ik}.
	\item \autoref{sec:survey:PSO} surveys existing research on \gls{pso}s. 
	\item \autoref{sec:survey:combo} covers research using \gls{pso}s and other \gls{ci} techniques to solve \gls{ik}. 
	\item \autoref{sec:survey:summary} summarises the chapter and provides a brief introduction to the next chapter. 
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Inverse Kinematics} \label{sec:survey:IK}
\summary{This section surveys existing literature on \gls{ik}. } 

\gls{ik} originated from robotics \cite{buss2004intro} but it has also been applied to computer animation \cite{aristidou2009inverse}. Some difference between the applications of inverse kinematics in the fields of robotics and computer animation is that computer models can be more complex with larger degrees of freedom \cite{welman1989figures, zhao1994nonlinear,tolani2000realtime} and that animated models can have more complex constraints \cite{tolani2000realtime}. \gls{ik} in computer animation is most commonly applied to animating humans or other life-like creatures \cite{buss2004intro}.

A popular iterative approach to solving inverse kinematics uses the Jacobian matrix (a matrix of partial derivatives of the entire system) \cite{aristidou2009inverse}, which gives linear approximations to the IK problem \cite{aristidou2009inverse, berkaikbasics} as described in \autoref{subsec:solvingik}. Orin \cite{orin1984jacobian} introduced and compared several different methods for efficiently calculating the Jacobian for different joint types. 

Because the Jacobian is not always invertible, various methods to calculate or approximate the inverse Jacobian have been proposed. One technique  is to simply use the transpose of the Jacobian instead of the inverse  \cite{balestrino1984robust, wolovich1984computational}. This method eliminates the need for computationally expensive matrix inversions \cite{pechev2008noinverse} and gives predictable results \cite{welman1989figures}  but it does not perform well situations where the Jacobian matrix is close to singular or the target is unreachable \cite{buss2004intro,pechev2008noinverse}. 

A generalised inverse of the Jacobian can also be used \cite{buss2004intro,welman1989figures}. One generalized inverse that is commonly used is the Moore-Penrose Inverse \cite{buss2004intro, buss2005selectively, greville1959inverse}. Klein and Huang \cite{klein1983pseudoinverse} showed that this pseudo inverse (for a non-square matrix) will result in as little joint movement as possible. In situations where the target position is not reachable or the Jacobian matrix is singular, the pseudo inverse can be clamped to prevent large changes in joint angles \cite{buss2005selectively}.  Baillieul \cite{baillieul1985kinematic} proposed the extended Jacobian method that tracks a local minimum, to eliminate the problem of singular matrices. 

The Jacobian based solutions aim to solve all joint positions at the same time,  making the incorporation of constraints more difficult. \citet{welman1989figures} and \citet{buss2005selectively} stated that simply clamping joints to their limits often give adequate results in practice, but can also lead to unexpected configurations. \citet{welman1989figures} also investigated the use of a matrix based penalty function to prevent constraints from being  violated. 

The damped least squares (DLS) method was first used for IK by Wampler \cite{wampler1986manipulator} and Nakamura and Hanafusa \cite{nakamura1986inverse}. DLS is based on the Jacobian method \cite{buss2004intro} but uses a damping factor to prevent infeasible joint configurations. DLS's damping sacrifices some precision \cite{deo1995dls}. Buss \cite{buss2005selectively} proposed a generalization of DLS called selectively damped least squares (SDLS) that selectively dampens values based on the difficulty of reaching a target. 

\citet{wang1991ccd} proposed a heuristic based approach, cyclic coordinate descent (CCD), that aims to solve all joints individually. At every iteration, CCD traverses the kinematic chain from end to base and updates joints individually based on a heuristic function  \cite{welman1989figures, lander1998kine}. This algorithm is guaranteed to converge, but convergence time is hard to estimate \cite{welman1989figures}. Introducing local, joint based constraints into CCD is relatively simple as individual joint configurations can be clamped before the next joint is changed \cite{wang1991ccd,welman1989figures, lander1998kine}. 

Non-matrix-based approaches have also been used to solve inverse kinematics. Guez and Ahmad \cite{guez1988neuralnet} used a neural network to solve the IK problem in robotics. Genetic \nobreak{Algorithms} has also been used to solve the IK problem for robots\cite{parker1989ga}. \autoref{sec:survey:combo} \nobreak{discusses} more existing \gls{ci} approaches for solving \gls{ik}.

\section{PSO}\label{sec:survey:PSO}
\summary{This section surveys existing research on \gls{pso}s.} 

\gls{pso} is a swarm intelligence algorithm based on the social interaction of bird flocks or schools of fish \cite{eberhart1995PSO, kennedy1995PSO, clerc2002coefficients}. It uses particles (candidate solutions) that move through hyperspace towards their personal and neighbourhood best positions. 

\gls{pso} was first proposed by \citet{eberhart1995PSO, kennedy1995PSO} in 1995. The original \gls{pso} had two user determined variables, namely an acceleration constant and a maximum velocity \cite{eberhart1995PSO}, to control the movement of particles. \citet{shi1998inertia} introduced an inertia weight component to  remove the necessity for a maximum velocity and improve control over the search.  Various strategies were also developed to adjust the inertia weight over time for better performance \cite{poli2007particle}. \citet{clerc1999constriction} developed a similar, mathematically equivalent, approach \cite{apengel} that uses a constriction coefficient instead of the inertia weight. \citet{clerc2002coefficients} analysed different ways to implement the constriction coefficients and suggested common values to ensure convergence.  

Kennedy and Eberhart \cite{eberhart1995PSO, kennedy1995PSO} introduced the gbest (global best) and lbest (local best) topologies with the original PSO, but other dynamic and static topologies and their effect on the search results have been investigated \cite{de2009frankenstein,  kennedy1999topology, mendes2004topology, peram2003topology, poli2007particle, suganthan1999topology}.

Various strategies have been proposed to handle boundary constrained problems in PSOs. The simplest approaches are to not consider infeasible solutions for best positions or to reinitialize them with random values \cite{apengel}. Other constraint handling approaches include penalty functions \cite{parsopoulos2002particleconstraints}, repair methods \cite{hu2002constraint} or replacing infeasible particles with their personal best positions immediately \cite{elgallad2002params}.

PSOs have been applied to a wide range of problems \cite{kennedy2011applications,poli2007particle}, including inverse kinematics for robotics \cite{huang2012particle, rokbani2012pso, wen2008psohybrid}, training neural networks \cite{kennedy2011applications} and single-  and multi-objective  optimization problems \cite{ delValle2008PSOmoo, apengel, kennedy2011applications}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Solving IK with CI Techniques}\label{sec:survey:combo}
\summary{This section describes existing \gls{ci} techniques that have been used to solve \gls{ik}.}

A few \gls{ci} techniques has been applied to \gls{ik} in the field of robotics:
\begin{itemize}
	\item \citet{rokbani2012pso, rokbani2013IKPSO} used a \gls{pso} to solve  inverse kinematics for an articulated robot system. Their experiment solved the \gls{ik} for a limb with only 2 \gls{dof}.  \citet{huang2012particle} applied a \gls{pso} to a robotic limb with 7 \gls{dof}. 
	\item \citet{parker1989ga} used a \gls{ga} to solve \gls{ik} for a robot. The \gls{ga} used a combination of distance from the target position and the amount of movement as a fitness measure and incorporated joint boundary constraints.
	\item \citet{d2001learning} used Locally Weighted Projection Regression to solve the \gls{ik} for a humanoid robot. 
	\item \citet{guez1988neuralnet} and others \cite{mayorga2005ann, kuroe1994neural} trained neural networks to solve \gls{ik} for robot control. 
\end{itemize}

\section{Robotics and Computer Graphics}
\summary{This section describes the differences between the \gls{ik} problem in the fields of Robotics and Computer Graphics. }

As noted in the previous section, \gls{pso}s and other \gls{ci} techniques have been applied to the \gls{ik} problem in the field of robotics multiple times. There are however some significant differences between solving \gls{ik} for robotics and for animated models:
\begin{itemize}
	\item Computer models can be more complex with larger degrees of freedom (more \nobreak{redundant} joints) than robots \cite{welman1989figures, zhao1994nonlinear,tolani2000realtime}.
	\item Animated models can have more complex constraints \cite{tolani2000realtime} that are hard to incorporate into traditional \gls{ik} techniques.
	\item Animated models are more likely to have dependencies between different joints or bones \cite{tolani2000realtime}.
	\item Computer animation sometimes has an additional requirement that motion be realistic \cite{welman1989figures}.
	\item In computer graphics systems, errors are usually less critical than in robotics systems  \cite{tolani2000realtime}.
\end{itemize}

\section{Summary}
\label{sec:survey:summary}
\summary{This section summarizes the chapter and gives a brief introduction to the following chapter.}

The Jacobian \gls{ik} solver is an iterative, matrix-based approach to solving the \gls{ik} problem. The algorithm uses matrix inversions to iteratively move joints closer to the target position. Since all matrices are not invertible, various approaches have been suggested to find a suitable approximation of the inverted matrix.  

\gls{pso}, first proposed by \citet{kennedy1995PSO,eberhart1995PSO},  is a  swarm intelligence \nobreak{optimization} algorithm. A number of variations and improvements have been proposed \cite{de2009frankenstein,  kennedy1999topology, suganthan1999topology, poli2007particle, shi1998inertia}. 

The next chapter describes the experiment setup used to compare a PSO \gls{ik} solver with the Jacobian solver. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%