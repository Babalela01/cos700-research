import os
import pickle

from common import get_bone, bone_chains, get_armature, get_bone_list, update_pose, distance, positions, chains

def filename(_algorithm, _chain, _position):
    po = "_".join(map(str, _position))
    return _chain + "_[" + po + "]_" + _algorithm + ".pickle"

data_folder = "C:/Users/renet/Documents/Renette/Universiteit/COS700/3D/blender/out/"

def read_pickle(_algorithm, _chain, _position):
    with open(data_folder + filename(_algorithm, _chain, _position), "wb") as f:
        return pickle.load(f)



def get_fitness(angles, target):
    update_pose(bone_list, angles)
    return distance(target, end_effector.tail)


def get_best_solution(by_run, target):
    index = 1
    pos = by_run[0][-1][index]
    fitness = get_fitness(pos, target)
    for run in by_run:
        newpos = run[-1][index]
        newf = get_fitness(newpos, target)
        if newf < fitness:
            fitness = newf
            pos = newpos
    return pos


def jacobian_pos(chain, target):
    by_run = read_pickle("Jacobian", chain, target)
    return get_best_solution(by_run, target)


# best Fitness, best position, average fitness, cure best Particle, cure best fitness
def pso_pos(chain, target):
    by_run = read_pickle("PSO", chain, target)
    return get_best_solution(by_run, target)


folder = "../results/positions"
os.makedirs(folder, exist_ok=True)
armature = get_armature('Armature')
for chain in chains:
    end_effector = get_bone(bone_chains[chain + "_end"], armature)
    bone_list = get_bone_list(bone_chains[chain], armature)
    res = [(pso_pos(chain, target), jacobian_pos(chain, target)) for target in positions]

    with open("%s/%s_joints.pickle" % (folder, chain), "wb") as f:
        pickle.dump(res, f, protocol=pickle.HIGHEST_PROTOCOL)


print("done")

