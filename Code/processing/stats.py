import os
import pickle
import statistics

from processing.common import stats_folder, positions, filename, chains, data_folder, load_pickle


def iterative_mins(by_gen):
    mins = [by_gen[0][0]]
    runs = len(mins[0])
    for gen in by_gen[1:]:
        last = mins[-1]
        add = [*gen[0]]
        for i in range(runs):
            add[i] = min([last[i], add[i]])
        mins.append(add)
    return mins


def jacobian_stats(chain, position):
    algorithm = "Jacobian"
    by_run = read_pickle(algorithm, chain, position)
    by_gen = [[*zip(*x)] for x in zip(*by_run)]
    itmin = iterative_mins(by_gen)
    res = {
        "Averages": [statistics.mean(x[0]) for x in by_gen],
        "Averages stddev": [statistics.stdev(x[0]) for x in by_gen],
        "All best": itmin,
        "Average Best": [statistics.mean(x) for x in itmin],
        "Final best" : itmin[-1],
        'Final average': by_gen[-1][0],
    }
    newf = stats_folder + "stats_" + filename(algorithm, chain, position)
    with open(newf, "wb") as f:
        pickle.dump(res, f, protocol=pickle.HIGHEST_PROTOCOL)
    pass



def read_pickle(_algorithm, _chain, _position):
    return load_pickle(data_folder + filename(_algorithm, _chain, _position))
# best Fitness, best position, average fitness, cure best Particle, cure best fitness
def pso_stats(chain, position):
    algorithm = "PSO"
    by_run = read_pickle(algorithm, chain, position)
    by_gen = [[*zip(*x)] for x in zip(*by_run)]
    res = {
        "Averages": [statistics.mean(x[2]) for x in by_gen],
        "Averages stddev": [statistics.stdev(x[0]) for x in by_gen],
        "All best": [x[0] for x in by_gen],
        "Average Best": [statistics.mean(x[0]) for x in by_gen],
        "Final best" : by_gen[-1][0],
        'Final average': by_gen[-1][2],
    }
    newf = stats_folder + "stats_" + filename(algorithm, chain, position)
    with open(newf, "wb") as f:
        pickle.dump(res, f, protocol=pickle.HIGHEST_PROTOCOL)
    pass


os.makedirs(stats_folder, exist_ok=True)
for c in chains:
    for p in positions:
        jacobian_stats(c, p)
        pso_stats(c, p)


print("done")

