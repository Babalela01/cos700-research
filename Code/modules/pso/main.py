# Note: Fix round and round stuff..
# > 180/ <-180 --> +/- 360 ?
# Left and right side has the same boundaries
# Convert degrees tp radians
# Make PSO work in radians for simplicity/less calculations
from common import bone_chains
from pso.swarm import Swarm


# Add bn_RF/LF to front

class PSO:
    @staticmethod
    def run(target, bone_name_list, end_name, run_count=30, **kwargs):
        return Swarm(target, bone_name_list, end_name, **kwargs)(run_count)

    @staticmethod
    def run_chain(target: list, chain: str, **kwargs):
        return PSO.run(target, bone_chains[chain], bone_chains[chain + "_end"], **kwargs)
