import pickle

positions = [
    (5.9, -2.3, 8.4),
    (2.3, -1.8, 5.4),
    (3.1, -0.6, 2.13),
    (0.6, -2.2, -0.6),
    (0.6, 1.1, -0.6),
    (0.7, -5.4, -1.9),
    (2.5, 1.1, -1.9),
    (1.7, 1.1, -4.5)
]

chains = ["RH2", "RH", "RF", "LF", "LH", "LH2"]
algorithms = ["PSO", "Jacobian"]
results_folder = "../results/"
stats_folder = results_folder + "stats/"
graph_folder = results_folder + "graphs/"
table_folder = results_folder + "tables/"
data_folder = "C:/Users/renet/Documents/Renette/Universiteit/COS700/3D/blender/out/"

def filename(_algorithm, _chain, _position):
    po = "_".join(map(str, _position))
    return _chain + "_[" + po + "]_" + _algorithm + ".pickle"


def load_pickle(fname):
    with open(fname, "rb") as f:
        return pickle.load(f)