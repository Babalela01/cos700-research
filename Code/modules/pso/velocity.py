from random import random


class Velocity:
    def __init__(self, w=0.789244, c1=1.4296180, c2=1.4296180):
        self.w = w
        self.c1 = c1
        self.c2 = c2

        self.r1 = None
        self.r2 = None

    def __call__(self, p, s):

        lx = p.position
        lv = p.velocity
        pb = p.best_position
        sb = s.best_position
        res = [
            (self.w * lv[i] +
             self.c1 * random() * (pb[i] - lx[i]) +
             self.c2 * random() * (sb[i] - lx[i]))
            for i in range(s.dimensions)]
        # print("[" + ",".join("%5.2f" % x for x in p.velocity)+ "]", "[" + ",".join("%5.2f" % x for x in res) + "]")
        return res

    def start(self, dimensions):
        pass


    def get_settings(self):
        return {"w": self.w,
                "c1": self.c1,
                "c2": self.c2
                }
