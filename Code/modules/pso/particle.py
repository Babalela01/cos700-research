import math
import random


class Particle:
    def __init__(self, dimensions, boundary):
        self.position = [random.uniform(*boundary[i]) for i in range(dimensions)]
        self.velocity = [0] * dimensions
        self.dimensions = dimensions
        self.boundaries = boundary
        self.fitness = None
        self.best_fitness = math.inf
        self.best_position = None
        self.boundaries = boundary

    def is_feasible(self):
        # return reduce(lambda a, v: a and v in self.boundaries, self.position)
        feasible, i = True, 0
        while i < self.dimensions and feasible:
            feasible = self.boundaries[i][0] <= self.position[i] <= self.boundaries[i][1]
            i += 1
        return feasible

    def update_best(self):
        if self.fitness < self.best_fitness and self.is_feasible():
            self.best_position = self.position
            self.best_fitness = self.fitness

    def __lt__(self, other):
        return self.fitness < other.fitness

    def __gt__(self, other):
        return self.fitness > other.fitness

    def __eq__(self, other):
        return self.fitness == other.fitness

    def __str__(self):
        return str(self.position)

    def check_boundaries(self):
        for i, x in enumerate(self.position):
            if x < Constraints.MIN:
                self.position[i] += Constraints.FULL
            elif x > Constraints.MAX:
                self.position[i] -= Constraints.FULL
            pass


class BonesParticle(Particle):
    # bone_list - list of bones: name, axis, boundaries
    def __init__(self, bone_list, boundaries):
        """
        :param bone_list: List of tuples (bone, axis)
        """
        super().__init__(len(bone_list), boundaries)


class Constraints:
    MIN = math.radians(-180)
    MAX = math.radians(180)
    FULL = math.radians(360)