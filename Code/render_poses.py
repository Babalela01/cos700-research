import os
import pickle

import time

import bpy

from common import get_armature, get_bone_list, bone_chains, reset_model, update_pose, get_bone, distance, positions
from mathutils import Vector

folder = "../results/positions"
outfolder = "../results/renders/"

os.makedirs(outfolder, exist_ok=True)
abs_outfolder = os.path.abspath(outfolder)
chains = ["RH2", "RH", "RF", "LF", "LH", "LH2"]

def handle_chain(chain):
    fname = "%s/%s_joints.pickle" % (folder, chain)
    with open(fname, "rb") as f:
        positions = pickle.load(f)
    model = get_armature('Armature')
    bones = get_bone_list(bone_chains[chain], model)

    for target in range(len(positions)):
        configs = positions[target]
        pso = configs[0]
        jac = configs[1]

        reset_model(model)
        update_and_save("PSO", pso, bones, chain, target)
        reset_model(model)
        update_and_save("Jacobian", jac, bones, chain, target)


def update_and_save(algorithm: str, angles: list, bones:list, chain:str, target:int):
    end_effector = get_bone(bone_chains[chain + "_end"], get_armature('Armature'))
    bpy.data.scenes['Scene'].render.filepath = "%s/%s/target_%d_%s" % (abs_outfolder, chain,  target+1, algorithm,)
    target_vec = Vector(positions[target])
    update_pose(bones, angles)
    old = distance(end_effector.tail, target_vec)
    bpy.ops.render.render(write_still=True)
    newd = distance(end_effector.tail, target_vec)
    print("Target " + str(target+1))
    print("Distance A: " + str(old1))
    print("Distance B: " + str(old))
    print("Distance C: " + str(newd))
    if old != newd:
        print("ERROR!!\n\n\n")



def run():
    for ob in bpy.context.visible_objects:
        ob.animation_data_clear()
    bpy.context.scene.frame_set(1)

    for c in chains:
        handle_chain(c)

# run()
