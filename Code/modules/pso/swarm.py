import math
import statistics
from operator import add

from common import get_bone, get_armature, get_bone_list, reset_model, update_pose, world_matrix
from mathutils import Vector, Matrix
from pso.particle import Particle
from pso.velocity import Velocity


class Fitness:
    def __init__(self, target_global: Vector, end_effector_bone, bone_list: list, world_matrix: Matrix):
        """

        :param target_global: Target position in world coordinates
        :param end_effector_bone:  The endeffector bone. The tail position should reach the target
        :param bone_list: List of tuples: (bone, axis).
        :param world_matrix: The world_matrix for the armature
        """
        self.target_global = target_global
        # Target position needs to be converted from world to object space
        # Alternative solution is to convert endeffector position every time,
        # but that would require more calculations
        self.target = world_matrix.inverted() * target_global
        self.end_effector = end_effector_bone
        self.bone_list = bone_list
        self.world_matrix = world_matrix

    def __call__(self, particle: Particle) -> float:
        return update_pose(self.bone_list, particle.position, self.end_effector.tail, self.target)


class Swarm:
    def __init__(self,
                 target: list,
                 bone_name_list: list,
                 end_name: str,
                 armature_name: str = 'Armature',
                 swarm_size: int = 30,
                 iterations: int = 200):
        """
        :param target: Target position for end effector
        :param bone_name_list: List of tuples - bone name, axis name, boundary
        :param end_name: Name of end effector bone
        :param armature_name: Name of armature
        :param swarm_size: Size of the particle swarm
        :param iterations: Number of iterations
        :return:
        """

        self.armature = get_armature(armature_name)
        reset_model(self.armature)

        self.boundaries = [b[2] for b in bone_name_list]
        self.bone_list = get_bone_list(bone_name_list, self.armature)
        self.dimensions = len(bone_name_list)

        self.end_bone = get_bone(end_name, self.armature)

        self.particles = None
        self.iteration_index = None
        self.best_fitness = None
        self.best_position = None

        self.max_iterations = iterations
        self.velocity_func = Velocity()
        self.fitness_func = Fitness(Vector(target), self.end_bone, self.bone_list, world_matrix(self.armature))
        self.swarm_size = swarm_size

    def __call__(self, runs=50):
        return [self.run() for _ in range(runs)]

    def run(self):
        self.particles = [Particle(len(self.bone_list), self.boundaries) for i in range(self.swarm_size)]
        self.best_fitness = math.inf
        self.best_position = []
        self.velocity_func.start(self.dimensions)

        results = [self.iteration() for self.iteration_index in range(0, self.max_iterations)]
        update_pose(self.bone_list, self.best_position)
        print("Final pose updated, fitness %.2f %s" % (self.best_fitness, str(self.best_position)))
        return results

    def iteration(self):
        for p in self.particles:
            p.fitness = self.fitness_func(p)
            p.update_best()
            if p.best_fitness < self.best_fitness:
                self.best_position = p.best_position[:]
                self.best_fitness = p.best_fitness

        # update_pose(self.bone_list, self.best_position) - Only if you wanna add a keyframe
        print("PSO Iteration #%d: %.2f, %.2f, %.2f" % (self.iteration_index, self.best_fitness,
                                                       self.average_fitness(), self.current_best().fitness))
        # Add k
        fitness_ = self.best_fitness, self.best_position, self.average_fitness(), self.current_best(), self.current_best().fitness
        for p in self.particles:
            p.velocity = self.velocity_func(p, self)
            p.position = list(map(add, p.position, p.velocity))
            p.check_boundaries()
        return fitness_

    def average_best(self):
        return statistics.mean([v.best_fitness for v in self.particles])

    def average_fitness(self):
        return statistics.mean([v.fitness for v in self.particles])

    def all_best(self):
        return [v.best_fitness for v in self.particles]

    def all_fitness(self):
        return [v.fitness for v in self.particles]

    def current_best(self) -> Particle:
        return min(self.particles)
