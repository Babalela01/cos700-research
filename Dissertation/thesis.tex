%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%              Document Class               %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[12pt, a4paper, oneside]{report}
% remove comment, below, to compile only the specified chapter (for draft chapter compilations)
%\includeonly{chapters/introduction/main}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%   Basic Packages and Package Settings     %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{float}												% customised float support
\usepackage{fancyhdr}											% improved header and footer support
\usepackage{amssymb}												% American Mathematical Society symbols
\usepackage{amsmath}												% American Mathematical Society environments
\usepackage{url}
\usepackage{verbatim}											% multi-line comments
\usepackage{ifpdf}												% selective setup for PDF compilation
\usepackage[numbers,sort&compress,elide]{natbib}
\usepackage{textcomp}
\usepackage{tabularx}

%\renewcommand{\bibsection}{\chapter{References}\label{sec:references}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%          PDFLaTeX-Specific setup          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\ifpdf
\usepackage[acronym,nomain,nonumberlist]{glossaries}	
%
\usepackage{graphicx}
\DeclareGraphicsExtensions{.pdf,.png,.jpg, .PNG}	

\usepackage{pdfpages}											% required for PDF watermarking
\usepackage{epstopdf}											% automatic conversion of EPS images
%\usepackage[pdftex]{thumbpdf}									% thumbnail generation for PDF files
\usepackage{pdflscape}											% required by thumbpdf
\usepackage[pdftex,	bookmarks=true, bookmarksnumbered=true]{hyperref}

\usepackage[all]{hypcap}										% correct PDF figure links to top of image

\hypersetup{
	pdfhighlight=/Y,												% (option) no visual cue on clicking link
	pdffitwindow=true,											% (option)
	pdfstartview=Fit,												% (option) fit initial view to page
	plainpages=false,												% (option) prevent hyperref page number changes
	breaklinks=true,												% (option) allow link breaking across lines
	colorlinks=true,												% (option) color link text only (no borders)
	pageanchor=false,												% (option) turns off page referencing for title page
	linkcolor=blue,												% (option) internal link color
	citecolor=blue,												% (option) citation link color
	filecolor=blue,												% (option) file link color
	menucolor=blue,												% (option) Acrobat menu link color
	pagecolor=blue,												% (option) page link color
	urlcolor=blue,													% (option) URL link color
}


\hypersetup{
	pdftitle    = {Solving Inverse Kinematics with Particle Swarm Optimisation},
	pdfauthor   = {Renette Ros},
	pdfsubject  = {Inverse Kinematics and Particle Swarm Optimisation},
	pdfkeywords = {Inverse kinematics, PSO},
}

% force LaTeX-compliant spacing

\pdfadjustspacing=1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       DVI/PostScript-Specific setup       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\else

\usepackage{graphicx}											

\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                  Lengths                  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setlength{\textwidth}{158mm}
\setlength{\hoffset}{4.5mm}
\setlength{\headheight}{15pt}
\setlength{\unitlength}{1pt}
\setlength{\footnotesep}{5mm}

\ifpdf
	% ensure PDF is centered in display
	\setlength{\hoffset}{-10.5mm}
\else
	% add gutter margin for DVI/PostScript
	\setlength{\hoffset}{-10.5mm}
	\addtolength{\hoffset}{4.5mm}
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%               Line Spacing                %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newlength{\originalbaselineskip}
\setlength{\originalbaselineskip}{\baselineskip}
\linespread{1.3}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%               List Counters               %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcounter{listcount}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%        Figures and Floating Bodies        %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{subcaption}
\usepackage{wrapfig}
%\usepackage{placeins}

\renewcommand{\topfraction}{0.9}
\renewcommand{\bottomfraction}{0.0}
\renewcommand{\textfraction}{0.1}
\renewcommand{\floatpagefraction}{0.9}
\setcounter{topnumber}{1}
\setcounter{bottomnumber}{0}
\newfloat{graph}{tbp}{lgf}[chapter]
\floatname{graph}{Graph}
\DeclareCaptionSubType{graph}
\newfloat{algorithm}{tbp}{loa}[chapter]
\floatname{algorithm}{Algorithm}
\def\algorithmautorefname{Algorithm}
\def\graphautorefname{Graph}
\renewcommand{\chapterautorefname}{Chapter}
\renewcommand{\sectionautorefname}{Section}
\renewcommand{\subsectionautorefname}{Section}


% bold float caption numbers and reduced size captions
\makeatletter
\long\def\@makecaption#1#2{%
	\vskip\abovecaptionskip
	\sbox\@tempboxa{{\small{\bf #1:} #2}}%
	\ifdim \wd\@tempboxa >\hsize
		{\small{\bf #1:} #2\par}
	\else
		\hbox to\hsize{\hfil\box\@tempboxa\hfil}%
	\fi
	\vskip\belowcaptionskip
}
\renewcommand\floatc@plain[2]{
	\setbox\@tempboxa\hbox{\small{\bf #1:} #2}%
	\ifdim\wd\@tempboxa>\hsize
		{\small{\bf #1:} #2\par}
	\else
		\hbox to\hsize{\hfil\box\@tempboxa\hfil}\fi}
\makeatother

\newlength{\abovesubfiglabelskip}
\setlength{\abovesubfiglabelskip}{0.5\abovecaptionskip}
\newcommand{\algcontent}[1]{\centering{\fbox{\parbox[]{155mm}{
	\setlength{\parindent}{0.3cm}
	\vspace{0.3cm}
	\small{
		#1
	}
	\vspace{0.3cm}
}}}}
\newcommand{\summary}[1]{#1\vspace{0.2cm}}
\newcommand{\sq}[1]{\lq#1\rq}


\input{extra/glossary}
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\folder}{.}
\include{coverpages/main}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\ifpdf
\hypersetup{pageanchor=true}									% (option) turn page referencing back on for chapters
\fi

\pagestyle{fancy}
\fancypagestyle{headings}{
	\fancyhead[RO,LE]{\thepage}
	\fancyhead[LO]{\sf\nouppercase{\leftmark}}
	\fancyhead[RE]{\sf\nouppercase{\rightmark}}
	\fancyfoot{}
	\renewcommand{\headrulewidth}{0.4pt}
}

\include{chapters/introduction/main}
\include{chapters/survey/main} % rename as required (but remember to update directories appropriately)
\include{chapters/method/main} % rename as required (but remember to update directories appropriately)
% add all other chapters you decide to write (also create an appropriate directory structure)
\include{chapters/results/main}
\include{chapters/conclusions/main}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\cleardoublepage
\ifpdf
\phantomsection
\fi
\label{bibliography}
\addcontentsline{toc}{chapter}{Bibliography}
\bibliographystyle{plainnat}
\bibliography{thesis}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\appendix
%\include{appendices/appendix1/main}
%\include{appendices/appendix2/main}
% any further appendices you decide to write (create appropriate directory structure, as with chapters)
\include{appendices/acronyms/main}
\include{appendices/symbols/main}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}