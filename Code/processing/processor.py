import os
import statistics

# {
#     "boundary": ...,
#     "dimensions": ...,
#     "max-iterations": ...,
#     "problem": ...,
#     "velocity": ...,
#     "comparator": ...,
#     "swarm-size": ...,
#     "run-count": ...,
#     "name": ...,
#     "run-data": [
#        [ {"gbest": ..., "avg_pbest": ..., "avg_fitness": ..., "swarm_best": ...}, ... ]
#        [ {"gbest": ..., "avg_pbest": ..., "avg_fitness": ..., "swarm_best": ...}, ... ]
#        ....
#      ]
# }

# Accuracy (Fitness of solution)
# Efficiency - Timet taken to reach solution
# Consistency - Deviation over multiple runs
from scipy.stats import mannwhitneyu
from processing.common import algorithms, chains, positions, filename, stats_folder, load_pickle, table_folder


def do_manU(x1, x2):
    muw = mannwhitneyu(x1, x2)
    p = muw[1] * 2
    return p




# A small p-value (< 0.05) indicates strong evidence against the null hypothesis, so it is rejected
def fmtp(p):
    str = "{:.4f}".format(p)
    if p < 0.05:
        return r"\textbf{" + str + "}"
    else:
        return str


def avg(pop):
    return (statistics.mean(pop), statistics.stdev(pop))


def format_avgs(a1, a2):
    numfmt = "${:.3f}_{{{:.3f}}}$"
    boldfmt = r"$\mathbf{{" + numfmt[1:-1] + "}}$"
    f1 = numfmt.format(*a1)
    f2 = numfmt.format(*a2)
    if a1[0] < a2[0]:
        f1 = boldfmt.format(*a1)
    elif a2[0] < a1[0]:
        f2 = boldfmt.format(*a2)

    return f1 + " & " + f2

def component(pop1, pop2):
    return format_avgs(avg(pop1), avg(pop2)) + " & " + fmtp(do_manU(pop1, pop2))

outpath = ""
# Fitness and  std dev at last generation
def main():
    headings = r"\hline  & \multicolumn{3}{c|}{\textbf{Best Fitness}} & " \
               r"\multicolumn{3}{c|}{\textbf{Average Fitness}} \\" + "\n" \
               r" & PSO & Jacobian & $p$ & PSO & Jacobian & $p$ \\ \hline" + "\n"
    for chain in chains:
        tbl = r"\begin{table}[!htbp] \centering" + "\n" \
                   + r"\begin{tabular}{|l| c c c | c c c |}" + "\n" \
                   + headings
        for targetid in range(len(positions)):
            label = 'Target {:d}'.format(targetid+1)
            resJ = load_pickle(stats_folder + "stats_" + filename("Jacobian", chain, positions[targetid]))
            resP = load_pickle(stats_folder + "stats_" + filename("PSO", chain, positions[targetid]))
            best_label = 'Final best'
            bestpart = component(resP[best_label], resJ[best_label])
            avg_label = 'Final average'
            avgpart = component(resP[avg_label], resJ[avg_label])
            tbl += label + " & " + bestpart + " & " + avgpart + r"\\" + "\n"
        tbl += r"\hline\end{tabular}" + "\n" \
               r"\caption{" + chain + r" Chain - Final iteration\\}\label{table:results:" + chain + "}\n" \
               r"\end{table}"
        with open(table_folder + chain + ".tex", "w") as f:
            f.write(tbl)

os.makedirs(table_folder, exist_ok=True)
main()
