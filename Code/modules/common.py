import math
from collections import OrderedDict
from typing import List, Tuple, Dict

import bpy
from mathutils import Vector, Matrix


def inv_boundaries(boundaries, axis):
    if axis != "x":
        boundaries = [-boundaries[1], -boundaries[0]]
    return map(math.radians, boundaries)


def get_inv_iterator(prefix, bones: Dict[str, Dict[str, Tuple[float, float]]]) -> List[
    Tuple[str, str, Tuple[float, float]]]:
    return ((prefix + bone, axis, [*inv_boundaries(bones[bone][axis], axis)]) for bone in bones for axis in
            bones[bone])


def get_iterator(prefix, bones: Dict[str, Dict[str, Tuple[float, float]]]) -> List[
    Tuple[str, str, Tuple[float, float]]]:
    return ((prefix + bone, axis, [*map(math.radians, bones[bone][axis])]) for bone in bones for axis in
            bones[bone])


spine = OrderedDict([
    ("Spine", {"x": [-5, 80], "y": [-20, 20], "z": [-10, 10]}),
])
# Boundaries for model's right side (Labelled L in Human.blend)
arm_list = OrderedDict([  # invert y,z
    ("Bicep", {"x": (-360, 360), "z": [-33, 55], "y": [0, 120]}),
    ("Forearm", {"x": (-140, 10)}),
    ("Wrist", {"x": (-60, 60), "y": [-60, 60]}),
    #("Hand", {"x": (-20, 20)})
])
arm_end = "Hand"
leg_list = OrderedDict([  # Keep x, invert y, z
    ("Thigh", {"x": [-80, 30], "z": [-15, 15], "y": [-60, 60]}),
    ("Shin", {"x": [0, 160]}),
    ("Ankle", {"x": [-10, 80]})
])
foot_end = "Foot"
# Model L/R bones are named the wrong way round in the model
bone_chains = {"LH": [*get_inv_iterator("bn_RH_", arm_list)],
               "LH2": [*get_iterator("bn_", spine), *get_inv_iterator("bn_RH_", arm_list)],
               "LF": [*get_inv_iterator("bn_RF_", leg_list)],
                "LH_end": "bn_RH_" + arm_end,
               "LH2_end": "bn_RH_" + arm_end,
               "LF_end": "bn_RF_" + foot_end,

               "RH": [*get_iterator("bn_LH_", arm_list)],
               "RH2": [*get_iterator("bn_", spine), *get_iterator("bn_LH_", arm_list)],
               "RF": [*get_iterator("bn_LF_", leg_list)],
               "RH_end": "bn_LH_" + arm_end,
               "RH2_end": "bn_LH_" + arm_end,
               "RF_end": "bn_LF_" + foot_end,
               }


def get_bone(name: str, armature: bpy.types.Object) -> bpy.types.PoseBone:
    bn = armature.pose.bones[name]
    bn.rotation_mode = 'XYZ'
    return bn


def get_axis(axis_name: str) -> int:
    """
    Maps X, Y, Z to 0,1,2
    :param axis_name:
    :return: axis number
    """
    return ord(axis_name.upper()) - ord('X')


def get_armature(armature_name: str) -> bpy.types.Object:
    return bpy.data.objects[armature_name]


def get_bone_list(bone_name_list: List[Tuple[str, str, Tuple[float, float]]],
                  armature: bpy.types.Object) -> List[Tuple[bpy.types.PoseBone, int]]:
    return [(get_bone(b[0], armature), get_axis(b[1])) for b in bone_name_list]


def add_keyframe(bone_list, frame):
    for pbone in bone_list:
        success = pbone[0].keyframe_insert(data_path="rotation_euler", frame=frame)
        return success


def reset_model(armature):
    activate_object(armature)
    # armature.animation_data_clear()
    # bpy.context.scene.frame_set(1)
    # bpy.ops.anim.change_frame(frame=1)
    for bn in armature.pose.bones:
        bn.rotation_mode = 'XYZ'
        bn.rotation_euler = [0, 0, 0]


def distance(pos: Vector, target: Vector) -> float:
    # return math.sqrt((pos[0] - position[0]) ** 2 + (pos[1] - position[1]) ** 2 + (pos[2] - position[2]) ** 2)
    return (pos - target).magnitude


def world_matrix(armature) -> Matrix:
    # The world matrix is necceeray to convert everything to the same coordinate system
    # we can get the object from the pose bone
    return armature.matrix_world


def activate_object(armature):
    bpy.context.scene.objects.active = armature
    bpy.ops.object.mode_set(mode='POSE')


def update_pose(bone_list, angles, end_effecotr=None, target=None):
    bpy.ops.object.mode_set(mode='POSE')
    for i, bone in enumerate(bone_list):
        pbone = bone[0]
        axis = bone[1]
        pbone.rotation_euler[axis] = angles[i]
    try:
        return distance(end_effecotr, target)
    except:
        pass


def get_rotation(bone: bpy.types.PoseBone, axis: int) -> float:
    return bone.rotation_euler[axis]


POSE_FRAMES = 24

positions = [
        (5.9, -2.3, 8.4),
        (2.3, -1.8, 5.4),
         (3.1, -0.6, 2.13),
         (0.6, 1.1, -0.6),
         (0.6, -2.2, -0.6),
         (2.5, 1.1, -1.9),
         (0.7, -5.4, -1.9),
         (1.7, 1.1, -4.5)
    ]