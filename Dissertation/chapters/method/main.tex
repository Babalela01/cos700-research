%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\renewcommand{\folder}{chapters/method/}
\chapter{Research Methodology}
\label{chap:method}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This chapter describes the experiment conducted to compare the \gls{pso} \gls{ik} solver to the Jacobian solver. Its structure is as follows:
\begin{itemize}
	\item \autoref{sec:method:software} describes the software, programming languages and libraries used. 
	\item \autoref{sec:method:model} and \autoref{sec:method:skeleton} discuss the model and its kinematic skeleton. 
	\item \autoref{sec:method:chains} gives a brief description of the kinematic chains that were tested. 
	\item \autoref{sec:method:targets} covers the target positions used to evaluate the algorithms. 
	\item \autoref{sec:method:common} lists parameter settings that are the same for both the \gls{pso} and the Jacobian solver. 
	\item \autoref{sec:method:PSO} describes the \gls{pso} implementation. 
	\item \autoref{sec:method:jacobian} discusses the Jacobian solver. 
	\item \autoref{sec:method:summary} summarises the chapter. 
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Software}\label{sec:method:software}
\summary{This section describes the software and libraries used for this research.}

The human model (\autoref{sec:method:model}) was originally built in \gls{3dsmax} \cite{3dsmax} due to familiarity with the modelling system. Although \gls{3dsmax} supports scripting in the built-in MaxScript \cite{autodesk2016script}, the model was exported to Blender \cite{blender}, an open source modelling program. Blender supports scripting in Python \cite{python,blenderScripting}, which is more widely used than Autodesk's MAXScript and has libraries that support the matrix calculations required for the Jacobian solver. 

The \gls{numpy} library for scientific computing was used to calculate the Moore-Penrose pseudo-inverse \cite{buss2004intro,greville1959inverse} and to transpose an invert matrices. The \texttt{MathUtils} module from Blender was used for matrix and vector types and to perform matrix multiplication. 


\section{The Model}
\label{sec:method:model}
\summary{This section describes the creation of the \gls{3d} human model. }

A human model was built in \gls*{3dsmax} using a reference image of a human male (\autoref{fig:ref}) based on the work of artist Andrew Loomis \cite{MonahangOrthoHuman} .  

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{\folder/reference.jpg}
	\caption{A reference image of a human male \cite{MonahangOrthoHuman}}\label{fig:ref}
\end{figure}

\autoref{fig:model} shows the completed model in rest position. This model was imported into Blender to create the kinematic skeleton (\autoref{fig:skeleton}) and implement the \gls{ik} solvers.

\begin{figure}[ht]
	\centering
	\begin{subfigure}{0.4\textwidth}
	\includegraphics[width=\textwidth]{\folder/model}
	\caption{The model}\label{fig:model}
	\end{subfigure}~\begin{subfigure}{0.4\textwidth}
	\includegraphics[width=\textwidth]{\folder/skeleton}
	\caption{The kinematic skeleton}\label{fig:skeleton}
\end{subfigure}
\caption{The human model}\label{fig:skelemodel}
\end{figure}

Blender's world coordinate system, as shown in \autoref{fig:skelemodel} and subsequent figures in this chapter is as follows:
\begin{itemize}
	\item The positive z-axis (blue line) points upwards.
	\item In the front views, the positive x-axis (red line) points to the right and the positive y-axis (green line) backwards.
	\item In the side views, the positive y-axis (green line) points to the right and the positive x-axis (red line) forwards.
	\item It is a right-hand coordinate system so positive rotation in counter-clockwise. 
\end{itemize}
All side views are shown from the right. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Kinematic Skeleton}
\label{sec:method:skeleton}
This section describes the creation, structure and constraints of the kinematic skeleton.

\subsection{Creation}
The kinematic skeleton, \autoref{fig:skeleton},  was built inside the Blender model and the model was skinned to move it's vertices with the bones. The skeleton is a tree-like structure with the base below the spine. %TODO Add reff
This means that changes in the leg chains do not affect the position of the arm chains and vice versa. Every bone, except the base, has a head that is connected to the tail of the previous bone forming a tree-like structure. 

Blender uses a right-hand coordinate system with the y-axis of every bone pointing along the length of the bone. This means y-axis rotation twists the bone, x-axis rotation moves the bone forwards and backwards and z-axis rotation moves the bone sideways. Every bone can rotate around its head position to simulate rotational joints. When using the \gls{bpy} \gls{api} bone rotations are specified in radians as an offset from the rest position.  

\subsection{Constraints}
Only boundary constraints on the joint angles were investigated in this study. This decision was made since the \gls{pso} and the Jacobian need to incorporate the same constraints to be comparable. Many constraint handling approaches have been developed for \gls{pso}s \cite{hu2002constraint}, but incorporating constraints into the approximated Jacobian is harder \cite{welman1989figures}.  Exact constraint values for the different joints are listed in the next subsection. 

\subsection{Skeleton bones}
The bones of the skeleton are listed below.  Rotation boundary constraints are indicated for the model's right side (where applicable). Due to the symmetry of the model and its possible movement, rotation boundaries around the y- and z-axis were inverted for the other side.
\begin{enumerate}
	\item Base -- no movement.
	\item Hips -- no movement, connects legs to base.
	\item Thighs (hip rotation): \begin{itemize}
		\item X-axis: [-80\textdegree,30\textdegree].
		\item Z-axis: [-15\textdegree,15\textdegree].
		\item Y-axis: [-60\textdegree, 60\textdegree].
	\end{itemize}

	\item Shins (knee rotation) \begin{itemize} 
		\item X-axis: [0\textdegree, 160\textdegree].
	\end{itemize}
	\item Ankles \begin{itemize} 
		\item X-axis: [-10\textdegree, 80\textdegree].
	\end{itemize}
	\item Feet -- end effectors of leg chains, no movement.
	\item Spine (not both sides):~\begin{itemize}
		\item X-axis: [-5\textdegree, 80\textdegree].
		\item Y-axis: [-20\textdegree,20\textdegree].
		\item Z-axis: [-10\textdegree, 10\textdegree].
	\end{itemize}

	\item Shoulders -- no movement, connects arms to spine.
	\item Upper arms (shoulder rotation): \begin{itemize}
		\item X-axis: No constraint.
		\item Z-axis: [-33\textdegree, 55\textdegree].
		\item Y-axis: [0\textdegree, 120\textdegree].
	\end{itemize}
	\item Lower Arms (elbow rotation) \begin{itemize}
		\item  X-axis: [-140\textdegree,10\textdegree].
		\end{itemize} \pagebreak[3]
	\item Wrists \begin{itemize}
		\item X-axis: [-60\textdegree, 60\textdegree].
	\end{itemize} 
	\item Hands -- end effectors for the arm chains, no movement. 

	\item Neck -- no movement. 
	\item Head -- no movement. 
\end{enumerate}

Boundaries are listed in degrees, but are converted to radians for usage with the Blender \gls{api}. 

\section{Kinematic Chains} \label{sec:method:chains}
\summary{This section describes the kinematic chains that were used to evaluate the \gls{ik} solvers. }

The following bone chains were tested:
\begin{itemize}
	\item Arms (Upper arm, lower arm, wrist) -- 6 \gls{dof}
	\item Arms with spine movement -- 9 \gls{dof}
	\item Legs (Thigh, shin, ankle) -- 5  \gls{dof}
\end{itemize}

Each of these chains is present on both sides of the model. \autoref{table:chainnames} shows the labels used for the chains during the experiment and in the results. Where left and right is used, it refers to the human model's left and right side. The different kinematic chains were evaluated independently and could therefore not affect the results of other chains. 

\begin{table}[!htbp]
 	\centering
 	\begin{tabular}{|l|l|}
 		\hline
		\textbf{Label} & \textbf{Chain description} \\ \hline
		LH & Left arm \\
		LH2 & Left arm with spine movement \\
		RH & Right arm \\
		RH2 & Right arm with spine movement \\
		LF & Left leg \\
		RF & Right Leg \\ \hline
		
 	\end{tabular}
 \caption{Labels used for kinematic chains}\label{table:chainnames}
 \end{table}

Since there are both positive and negative boundary constraints, all angles were also constrained to the range [-180\textdegree, 180\textdegree] for both solvers. Joint rotations that fell outside this range was simply wrapped around by adding or subtracting 360\textdegree, changing the numerical value but keeping the actual rotation the same. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Target Positions}\label{sec:method:targets}
\summary{This section lists and describes the target positions that were used to evaluate the \gls{ik} solvers.}

All kinematic chains were tested with eight different target positions.  \autoref{table:positions} lists the target positions with the chains and indicates whether the target position can be reached with the different kinematic chains. An \sq{Y} indicates that it can be reached and a \sq{N} indicates that it cannot be reached. \autoref{fig:positions} shows the positions with the model.
\begin{table}[htbp]
	\centering
	\caption{Target Positions}\label{table:positions}
	\begin{tabular}{|c|l|c c c c c c|}
		\hline
		& \textbf{Point} & \multicolumn{6}{c|}{\textbf{Reachable}} \\ 
		&                 &LH &LH2&RH &RH2&LF &RF \\\hline
		1. &(5.9, -2.3, 8.4) & N & N & N & N & N & N \\
		2. &(2.3, -1.8, 5.4) & Y & Y & Y & Y & N & N \\
		3. &(3.1, -0.6, 2.13 & Y & Y & N & N & N & N \\
		4. &(0.6, -2.2, -0.6)& N & Y & N & Y & Y & Y \\
		5. &(0.6, 1.1, -0.6) & N & Y & N & Y & Y & Y \\
		6. &(0.7, -5.4, -1.9)& N & N & N & N & N & N \\
		7. &(2.5, 1.1, -1.9) & N & Y & N & Y & Y & Y \\
		8. &(1.7, 1.1, -4.5) & N & N & N & N & N & N \\ \hline
	\end{tabular}
\end{table}

\begin{figure}[!htbp]
	\centering
	\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\textwidth]{\folder/targets_front}
		\caption{Front view}
	\end{subfigure}~\begin{subfigure}{0.4\textwidth}
		\includegraphics[width=\textwidth]{\folder/targets_side}
		\caption{Side view}
	\end{subfigure}
	\caption{The target positions}\label{fig:positions}
\end{figure}

\pagebreak
Target positions are listed in world space. Before evaluating the fitness or calculating the Jacobian, it was converted to the object-space of the human model by multiplying it with the inverse of the model's world matrix -- the matrix used for transformations from object space to world space.  The model's world matrix is obtained through the blender \gls{api} as \texttt{armature.matrix\_world}. This conversion prevents incorrect fitness or matrix calculations when a transformation was applied to the entire model.  If no transformations were applied to the model, the object space is the same as the world space. 

 
\section{Common Parameter Settings} \label{sec:method:common}
\summary{This sections describes parameter settings that were the same for both solvers.}

Some parameter settings were common between the \gls{pso} and the Jacobian solver:
\begin{itemize}
	\item Maximum iterations: 200
	\item Number of independent runs (for every chain/target combination): 30
	\item Stopping Condition: The only stopping condition for both algorithms was when the maximum number of iterations were reached. 
	\item Constraint Handling: Boundary constraints were handled by clamping out of bounds angles to the boundaries after the joint angles were updated \cite{xu2007boundaries, baerlocher2004inverse}.
\end{itemize}


\section{The PSO}
\label{sec:method:PSO}
This section describes the \gls{pso} algorithm used to solve the \gls{ik} for the human model. \autoref{sec:method:pso} discusses the \gls{pso} variation and the chosen topology,  \autoref{sec:method:psoparams} describes parameter settings and particle initialization, \autoref{sec:method:psoparticle} describes the particle structure and \autoref{sec:method:psofitness} describes the fitness function. \autoref{sec:method:psoalg} summarises the algorithm for the complete \gls{pso} \gls{ik} solver.

\subsection{PSO algorithm and topology}\label{sec:method:pso}
A global-best \gls{pso} \cite{kennedy1995PSO} with an inertia weight \cite{shi1998inertia} was used. Particle velocities ($\vec{v_{i}}$) and positions ($\vec{x_{i}}$) were updated as shown in \autoref{eq:pso:velocity} and \autoref{eq:pso:pos} \cite{shi1998inertia, apengel}.  The \gls{pso} used the distance of a chain's end-effector from a target position as the fitness value. Chosen parameter values are discussed in \autoref{sec:method:psoparams}.

\begin{equation}\label{eq:pso:velocity}
\vec{v_{i}} = w\vec{v_{i}} + c_1\vec{r_1}\circ(\vec{p_{i}}-\vec{x_{i}}) + c_2\vec{r_2}\circ(\vec{p_{g}}-\vec{x_{i}})
\end{equation}

\begin{equation}\label{eq:pso:pos}
\vec{x_{i}} = \vec{x_{i}} + \vec{v_{i}}
\end{equation}

\subsection{Parameters} \label{sec:method:psoparams}
 The following parameter values were used: 
\begin{itemize}
	\item Inertia weight $w=0.789244$
	\item $c_1=1.4296180$
	\item $c_2=1.4296180$
	\item Population size: 30 %TODO source
\end{itemize}
The values chosen for $w$, $c_1$ and $c_2$ have been shown to ensure convergence in common optimization problems \cite{apengel, trelea2003parameters}. 

Particles were initialized to random values within the boundaries and the particle velocity was initialized to 0. 

\subsection{Particle} \label{sec:method:psoparticle}
Every particle in the \gls{pso} was a set of angles in radians. Every element of the particle corresponded to an angle and an axis of a bone in the chain. The dimensions of the problem was therefore the total \gls{dof} of the kinematic chain -- the sum of the \gls{dof}s of all the individual joints in the chain. This approach is similar to the one used by \citet{rokbani2013IKPSO}.

\subsection{Fitness} \label{sec:method:psofitness}
The fitness function used in the \gls{pso} is the euclidean distance of a chain's end-effector from the target position \cite{rokbani2013IKPSO}. The end-effector position was retrieved from the Blender Python \gls{api} after setting all bone rotations from a particle. 

\subsection{Algorithm} \label{sec:method:psoalg}
\autoref{alg:pso} summarises the approach used for the entire \gls{pso} \gls{ik} solver. 
\begin{algorithm}[H]
	\algcontent{
		Convert target position from world space to object space.\par
		Initialize all particles to random positions with 0 velocity.\par
		\textbf{repeat:}\par
		\hspace{1cm} Calculate the fitness of every particle.\par
		\hspace{1cm} Update the global and personal best positions.\par
		\hspace{1cm} Update particle velocities (\autoref{eq:pso:velocity}).\par
		\hspace{1cm} Update particle positions (\autoref{eq:pso:pos}). \par
		\hspace{1cm} Correct  out-of-bounds joint angles. \par
		\textbf{until stopping condition reached}
	}
	\caption{Solving inverse kinematics with a \gls{pso}}
	\label{alg:pso}
\end{algorithm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{The Jacobian}
\label{sec:method:jacobian}
This section describes the Jacobian \gls{ik} solver. \autoref{sec:method:jacinit} describes how joints were initialized, \autoref{sec:method:jaccalc} discusses how the Jacobian matrix was calculated and \autoref{sec:method:jacinverse} describes how the inverse Jacobian was approximated. \autoref{sec:method:jacalg} summarises the algorithm for the complete Jacobian \gls{ik} solver. 

\subsection{Initialization} \label{sec:method:jacinit}
Joint angles were initialized to random values within the boundaries to test the performance of the Jacobian solver independent of its starting position. 

\subsection{Calculating the Jacobian} \label{sec:method:jaccalc}
Instead of calculating partial derivatives to determine the elements of the Jacobian Matrix, a simplified equation was used. 

Every element of the Jacobian matrix was calculated with \autoref{eq:jacobian} \cite{buss2004intro} by using the joint position ($p_j$), end-effector position ($s_j$) and rotation axis ($v_j$). Rotational joints with more than one \gls{dof} were treated as separate joints at the same position with a single \gls{dof} each.
 
\begin{equation}\label{eq:jacobian}
\frac{\partial{s_i}}{\partial{\theta_i}}=\vec{v_j}  \times  (\vec{s_i}-\vec{p_j})
\end{equation}

The unit vector $\vec{v_j}$ was obtained from the Blender \gls{api} as \texttt{bone.x\_axis}, \texttt{bone.y\_axis} or \texttt{bone.z\_axis} (depending on the axis of rotation) where \texttt{bone} is the bone (\texttt{bpy.types.PoseBone}) whose head defines the joint. 

\subsection{Inversion} \label{sec:method:jacinverse}
The Moore-Penrose pseudo-inverse \cite{greville1959inverse, Girard1985modelling}, a generalised matrix inverse, was used to approximate the inverse Jacobian. The \gls{numpy} library provides an implementation  that was used to calculate it. 

\subsection{Algorithm}\label{sec:method:jacalg}
\autoref{alg:jacobiansolution} summarises the approach used for the Jacobian solver. 

\begin{algorithm}[!htpb]
	\algcontent{
		Convert target position from world space to object space.\par
		Initialize joint angles to random values.\par
		\textbf{repeat:}\par
		\hspace{1cm} Calculate the Jacobian (\autoref{eq:jacobian}).\par
		\hspace{1cm} Get the inverse of the Jacobian.\par
		\hspace{1cm} Calculate the angle update values (\autoref{eq:deltathe}).\par
		\hspace{1cm} Update joint angles. \par
		\hspace{1cm} Correct joint angles for constraints. \par
		\textbf{until stopping condition reached}
	}
	
	\caption{Solving inverse kinematics with a Jacobian matrix}
	\label{alg:jacobiansolution}
\end{algorithm}

\section{Analysing Results}\label{sec:method:results}
\summary{This section describes how results were logged and analysed.}

During execution of the simulations, the best and average fitness at every iteration and the final solutions of both algorithms were logged for later analysis. 

A Man-Whitney U test \cite{mannnachar2008, mannwhitney} was used on the average and best solutions at the final iteration to compare the PSO with the Jacobian solver. 
The Mann-Whitney U test compares two independent, random populations and attempts to disprove the null hypothesis -- that the two populations were sampled from identical distributions. The probability (p-value) is obtained from tables with the calculated statistic value and population sizes \cite{mannnachar2008, mannwhitney}. This implementation used the SciPy (Scientific computing library for python) library's Mann-Whitney U function which calculates and returns p with the statistic value. 

 A significance level of 0.05 was used: If $p < 0.05$ the null hypothesis is rejected and the differences between the two populations are considered significant. 

\section{Summary}
\label{sec:method:summary}
\summary{This section summarises this chapter and gives a brief introduction to the following chapter.}

A human model with a kinematic skeleton was built in Blender. Kinematic chains for the arms and legs were defined for the purpose of the experiment. Different target positions, some reachable and some not, were chosen to test the \gls{ik} solvers for every kinematic chain.  Inverse kinematics was implemented in Python for execution on this model. 
 
The experiment used a global-best PSO with inertia weight as described by \citet{shi1998inertia} and a Jacobian solver that used the Moore-Penrose pseudo inverse. The Jacobian matrix was approximated using a linear approximation described by  \citet{buss2004intro}. 

Since the PSO is a stochastic algorithm and the Jacobian solver used random initialization all results were analysed over 30 independent runs.  

The next chapter describes the results of the experiment. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%