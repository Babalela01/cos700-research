%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Conclusions and Further Work}
\label{chap:conclusions}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This chapter contains the conclusions of this work in \autoref{sec:conclude} and highlights possible further research in \autoref{sec:conclude:further}. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Summary and Conclusions}\label{sec:conclude}
\summary{This section summarises the work and contains the conclusions based on the results shown in the previous chapter.}

A human model with a kinematic skeleton was created in Blender. Blender was chosen because it is open source and it uses Python for scripting. The \gls{ik} could be solved with only the kinematic skeleton, but the attached model helped visualise the poses and joint constraints. The model and skeleton was built based on reference images of a typical human male. 

A \gls{pso} was developed to solve the \gls{ik} for the Blender model. The \gls{pso} was developed in Python and used the Blender \gls{api}. Particles encoded the joint configurations as a vector of angles in radians. Fitness was evaluated by simply updating the Blender model's pose and finding the distance between the end-effector of a kinematic chain and its target position. Target positions for all chains included positions that were reachable and positions that were far out of range. 

The Jacobian matrix is usually built from partial derivatives of the equation used to find the end-effector position. Instead of finding partial derivatives for every different kinematic chain, an approximation was used \cite{buss2004intro}. The Blender \gls{api} was used to find the end-effector position instead of developing a separate equation for every kinematic chain. As in the \gls{pso} solver, the Jacobian solver was scripted in Python. 

The only constraints that were investigated was boundary constraints on the joint angles. This was because the \gls{pso} and the Jacobian need to incorporate the same constraints to be comparable. Many constraint handling approaches have been developed for \gls{pso}s, but incorporating constraints into the approximated Jacobian is harder. 

The \gls{pso}'s fitness measure, the distance of an end-effector from the target position, was used to compare the performance of the two algorithms. Over the 48 simulations (6 kinematic chains evaluated on 8 target positions), the \gls{pso} found significantly  better final solutions than the approximated Jacobian for 45 of the simulations. Of the remaining three simulations, one simulation showed no significant difference and for the other two the Jacobian solver found significantly better solutions.

A problem with the \gls{pso} approach is that good solutions could potentially be missed -- none of the \gls{pso}'s final solutions, even for reachable positions, had a fitness $<0.2$. This inability to always exploit good solutions might be due to dependencies between joint angles: The angle of a joint high up in the chain influences the positions of joints closer to the end-effector. Another problem encountered was that, even though good solutions were found, the average population fitness of the \gls{pso} did not improve a lot over the first 200 iterations -- the population did not converge. This could possibly be due to parameter settings or the inter-component dependencies mentioned previously. 



Based on execution time, the \gls{pso} is probably not suited for real-time applications. For many of the simulations, the Jacobian solver found a good solution faster than the \gls{pso} even though the PSO found better solutions in the end. For applications that require some real-time calculations but also offline calculations, the two approaches could possibly be combined and the Jacobian solutions used as some of the initial particles for the offline calculations. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Future Work}\label{sec:conclude:further}
This section highlights possible further work based on the research conducted in this work: 

\begin{itemize}
	\item Investigate different fitness functions for the \gls{pso}:
	\begin{itemize}
		\item During an animation task, an appropriate addition might be to minimize joint movement between two keyframes. 
		\item Find some quantitative measure of the realism of a specific pose when modelling a human or living creature. 
	\end{itemize} 
	\item Investigate the performance of the \gls{pso} in an \gls{ik} system with more complex constraints. 
	\item Investigate \gls{ik} as a multi-objective optimisation problem: \begin{itemize}
		\item Using multiple fitness measures for a single kinematic chain. 
		\item Solve the \gls{ik} for multiple limbs at the same time. In this research, kinematic chains were evaluated independently so the single shared bone (the spine shared between the two arm chains) did not affect the movement of a chain other than the evaluated chain.
		  In more complex models the movement of one chain might influence the movement of another chain due to shared bones. 
	\end{itemize}
	\item This research considered a model with only rotational joints. Prismatic (sliding) joints \cite{buss2004intro, orin1984jacobian} could also be present in an \gls{ik} system. 
	\item Compare the performance of the \gls{pso} for solving \gls{ik} to other \gls{ci} algorithms, including evolutionary algorithms and other swarm intelligence algorithms. 
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%