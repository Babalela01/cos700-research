import os
import pickle


# import bpy
# bpy.ops.debug.connect_debugger_pycharm()

folder = "out"
def run_both(chain, target):
    print("Process:", chain, target)
    from jacobian.main import Jacobian

    run_count = 30
    results = Jacobian.run_chain(target, chain, run_count=run_count, max_iterations=200)
    outfile = filename(chain, target, "Jacobian")
    with open(outfile, 'wb') as handle:
        pickle.dump(results, handle, protocol=pickle.HIGHEST_PROTOCOL)

    from pso.main import PSO

    results = PSO.run_chain(target, chain, run_count=run_count, iterations=200)
    print(results)
    outfile = filename(chain, target,"PSO")
    with open(outfile, 'wb') as handle:
        pickle.dump(results, handle, protocol=pickle.HIGHEST_PROTOCOL)


def filename(chain, target, algorithm):

    return "%s/%s_[%s]_%s.pickle" % (folder, chain,  "_".join(map(str, target)), algorithm)


def main():
    from common import reset_model, get_armature
    from itertools import product
    os.makedirs(folder, exist_ok=True)

    print("Starting")
    reset_model(get_armature("Armature"))
    chains = ["LH2"]#["RH2" ,"RH", "RF", "LF", "LH", "LH2"]

    combs = product(chains, positions)
    for c in combs:
        run_both(*c)


main()
