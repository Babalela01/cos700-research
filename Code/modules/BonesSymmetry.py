from math import radians
import bpy


def symm(obj: bpy.types.Object):
    bones = obj.data.edit_bones
    bpy.context.scene.objects.active = obj
    bpy.ops.object.mode_set(mode='EDIT')
    for b in bones:
        b.roll = 0;
        on = b.name

        if on[3] == 'L':
            an = on[:3] + 'R' + on[4:]
            print(on + ", " + an)
            ab = bones[an]
            b.head[0] = -ab.head[0]
            b.head[1] = ab.head[1]
            b.head[2] = ab.head[2]
            b.tail[0] = -ab.tail[0]
            b.tail[1] = ab.tail[1]
            b.tail[2] = ab.tail[2]
    bpy.ops.object.mode_set(mode='POSE')


symm(bpy.data.objects['Armature'])


#########################


def rotate(bone, axis, angle):
    bone.rotation_euler[axis] = radians(angle)
