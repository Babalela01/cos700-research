import os
import pickle

import matplotlib.pyplot as plt
import pylab

from processing.common import positions, chains, algorithms, stats_folder, filename, graph_folder, load_pickle

cutoff = 200
settings = {
    "PSO": "-",
    "Jacobian": ":"
}


def read_pickle(_algorithm, _chain, _position):
    return load_pickle(stats_folder + "stats_" + filename(_algorithm, _chain, _position))


def generations_graph(data_groups, _title, file, _cutoff):
    figure = plt.figure(figsize=(10, 6), )
    plt.gca().set_prop_cycle(None)
    for _data in data_groups:
        column = _data[0]['title']
        plt.plot([0], marker='None', linestyle='None', label=column)  # Dummy plot for column headinhg
        for series in _data:
            plt.plot(series['data'][:_cutoff], series['settings'],
                     linewidth=1.0,
                     label=series['label'],
                     color=series['color'])
            # legends.append(plt.legend(lines, labels, _title=_title, loc=loc))

    plt.xlabel('Time (Iteration)')
    plt.ylabel('Average fitness')
    plt.title(_title)

    legend = plt.legend(ncol=2, columnspacing=-2, fontsize='x-small')
    figure.tight_layout()
    for t in legend.get_texts():
        if t._text in algorithms:
            t._x = -35

    plt.savefig(file)
    # plt.show()


def add_data(_chain, _target_id, _key, _color):
    ls = []
    for _a in algorithms:
        ls.append(algorithm_data(_a, _target_id, _chain, _key, _color))
    return ls


def algorithm_data(_algorithm, _targetid, _chain, _key, _color):
    _position = positions[_targetid]
    _settings = settings[_algorithm]
    data = read_pickle(_algorithm, _chain, _position)
    avg = data[_key]
    label = "Target " + str(_targetid+1)
    avgd = {"data": avg, "settings": _settings, "label": label if _algorithm == algorithms[1] else " ", "color": _color,
            "title": _algorithm}
    return avgd


def main():
    num_colors = len(positions)
    cm = pylab.get_cmap('gist_rainbow')

    os.makedirs(graph_folder, exist_ok=True)
    for chain in chains:
        draw_chain_graph(chain, "Averages", " Chain - average fitness over time", "avggraph",
                         get_colors(num_colors, cm))

        draw_chain_graph(chain, "Average Best", " Chain - best fitness over time", "bestgraph",
                         get_colors(num_colors, cm))


def get_colors(num, color_model):
    return (color_model(1. * i / num) for i in (range(num)))


def draw_chain_graph(chain, key, title_fragment, filename_part, colors):
    title = chain + title_fragment
    all_data = [add_data(chain, targetid, key, next(colors)) for targetid in range(len(positions))]
    data = [*zip(*all_data)]  # Re-order data to be grouped by algorithm

    generations_graph(data, title, "%s/%s_%s.png" % (graph_folder, chain, filename_part), cutoff)

os.makedirs(graph_folder, exist_ok=True)
main()
